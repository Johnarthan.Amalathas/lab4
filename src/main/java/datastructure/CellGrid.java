package datastructure;

import cellular.CellState;

import java.util.ArrayList;

public class CellGrid implements IGrid {
    // field variables
    private int rows;
    private int cols;
    /** Two dimentional array with cellstates(2d list)*/
    private CellState[][] states;

    public CellGrid(int rows, int columns, CellState initialState) {
        // constructor
        this.rows = rows;
        this.cols = columns;
        this.states = new CellState[rows][columns];

        for (int row = 0; row < this.rows ; row++) {
            for (int col = 0; col < this.cols; col++) {
                this.states[row][col] = initialState;

            }


        }
	}

    @Override
    public int numRows() {
        return this.rows;
    }

    @Override
    public int numColumns() {
        return this.cols;
    }

    @Override
    public void set(int row, int column, CellState element) {
        this.states[row][column] = element;
    }

    @Override
    public CellState get(int row, int column) {
        return this.states[row][column];
    }

    @Override
    public IGrid copy() {
        CellGrid myCopy = new CellGrid(this.rows, this.cols, null);
        for (int row = 0; row < this.rows; row++) {
            for (int col = 0; col < this.cols; col++) {
                myCopy.set(row, col, this.get(row, col));
            }

        }
        return myCopy;
    }
    
}
